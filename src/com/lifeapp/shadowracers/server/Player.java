package com.lifeapp.shadowracers.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.lifeapp.shadowracers.server.games.Game;
import com.lifeapp.shadowracers.server.messages.*;

public class Player implements Comparable<Player> {
    public Game game;
    public Connection connection;

    public int score = 0;

    public PlayerModel playerModel = new PlayerModel();

    public boolean ready = false;

    Player(Connection connection) {
        this.connection = connection;
        Listener listener = new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                if (object instanceof StateMessage) {
                    ((StateMessage) object).name = playerModel.name;
                    game.sendUdpExcept(object, Player.this);
                } else if (object instanceof RequestLobbyMessage) {
                    RequestLobbyMessage requestLobbyMessage = ((RequestLobbyMessage) object);
                    if (requestLobbyMessage.netVersion != KryServer.NET_VERSION) {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.code = 1;
                        sendTcp(errorMessage);
                        close();
                        return;
                    }
                    playerModel = requestLobbyMessage.playerModel;
                    KryServer.joinGame(-1, Player.this);
                } else if (object instanceof GoalReachedMessage) {
                    game.playerGoalReached(Player.this, (GoalReachedMessage) object);
                } else if (object instanceof PlayerReadyMessage) {
                    game.playerReady(Player.this);
                }
            }

            @Override
            public void disconnected(Connection connection) {
                if (game != null) {
                    game.exit(Player.this);
                }
                KryServer.removePlayer(Player.this);
            }
        };
        connection.addListener(listener);
    }

    public void sendTcp(Object object) {
        connection.sendTCP(object);
    }

    public void sendUdp(Object object) {
        connection.sendUDP(object);
    }

    public void close() {
        if (connection != null && connection.isConnected()) {
            connection.close();
        }
        KryServer.removePlayer(this);
    }


    public PlayerModel getModel() {
        return playerModel;
    }

    @Override
    public int compareTo(Player o) {
        return o.score - score;
    }
}
