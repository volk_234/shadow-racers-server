package com.lifeapp.shadowracers.server;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FireBaseUtils {
    public static void pushNewGameNotification() {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
        try {
            StringEntity stringEntity = new StringEntity("{ \"to\": \"/topics/newGame\", \"notification\": {     \"title\": \"Найдена новая игра!\",     \"body\": \"Присоединиться?\",     \"sound\": \"default\",     \"tag\": \"newGame\" }, \"data\": {     \"type\": \"newGame\" } }", "UTF-8");
            post.addHeader("content-type", "application/json");
            post.addHeader("Authorization", "key=" + Env.FIREBASE_KEY);
            post.setEntity(stringEntity);
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
