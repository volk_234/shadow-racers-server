package com.lifeapp.shadowracers.server.games;

import com.lifeapp.shadowracers.server.Player;
import com.lifeapp.shadowracers.server.PlayerModel;
import com.lifeapp.shadowracers.server.messages.GameInitMessage;
import com.lifeapp.shadowracers.server.messages.GoalReachedMessage;

public class GameLaps extends Game {

    private final int[][] tracks = {
            {1918, 1929, 1905, 1934, 1957, 1956, 1955, 1958, 1917, 1920, 1919},
            {1910, 1911, 1912, 1913, 1914, 1927, 1940, 1928, 1941},
            {1957, 1937, 1935, 1939, 1940, 1915, 1954, 1929, 1905, 1934},
    };
    private final int[][] availableSpawns = {
            {1968, 1969, 1967, 1970},
            {1972, 1973, 1971, 1974},
            {1976, 1977, 1975, 1978},
    };
    private int lapsCount;
    private int goalsCount;
    private int[] track;
    private int[] spawns;

    public GameLaps(int lapsCount) {
        this.lapsCount = lapsCount;
        int index = random.nextInt(tracks.length);
        track = tracks[index];
        spawns = availableSpawns[index];
        goalsCount = track.length * lapsCount;
    }

    @Override
    void init() {
        status = 1;
        GameInitMessage gameInitMessage = new GameInitMessage();

        gameInitMessage.spawns = spawns;
        gameInitMessage.mode = 2;
        gameInitMessage.goals = new int[1 + track.length];
        gameInitMessage.goals[0] = lapsCount;
        System.arraycopy(track, 0, gameInitMessage.goals, 1, track.length);

        gameInitMessage.playerModels = new PlayerModel[players.size()];
        for (int i = 0; i < players.size(); i++) {
            gameInitMessage.playerModels[i] = players.get(i).getModel();
        }
        sendTcp(gameInitMessage);

    }

    @Override
    void gameStarted() {

    }

    @Override
    public void playerGoalReached(Player player, GoalReachedMessage goalReachedMessage) {
        goalReachedMessage.name = player.playerModel.name;
        player.score += 1;
        if (player.score >= goalsCount) {
            win(player);
        } else {
            sendTcp(goalReachedMessage);
        }
    }
}
