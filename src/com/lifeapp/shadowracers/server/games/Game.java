package com.lifeapp.shadowracers.server.games;

import com.lifeapp.shadowracers.server.KryServer;
import com.lifeapp.shadowracers.server.Player;
import com.lifeapp.shadowracers.server.messages.*;

import java.util.*;

public abstract class Game {


    private final int playerCount = 4;
    final int maxGoalIndex = 25;

    Random random = new Random();
    private int id = random.nextInt();

    int status = 0;

    ArrayList<Player> players = new ArrayList<>();

    Timer timer = new Timer();
    private TimerTask longWaitTask;
    private boolean longWaitTaskScheduled = false;

    Game() {
    }

    abstract void init();

    abstract void gameStarted();

    private void start() {
        timer.schedule(new TimerTask() {
            int timeLeft = 5;

            @Override
            public void run() {
                timeLeft--;
                if (timeLeft <= 0) {
                    cancel();
                    gameStarted();
                }
                GameStartMessage gameStartMessage = new GameStartMessage();
                gameStartMessage.timeLeft = timeLeft;
                sendTcp(gameStartMessage);
            }
        }, 0, 1200);
    }

    void win(Player winner) {
        if (status == 2)
            return;
        status = 2;
        GameEndMessage gameEndMessage = new GameEndMessage();
        gameEndMessage.winner = winner.playerModel;
        sendTcp(gameEndMessage);
    }

    private void end() {
        timer.cancel();
        status = 2;
        closeConnections();
        KryServer.removeGame(this);
    }

    public void playerReady(Player player) {
        player.ready = true;
        for (Player player1 : players) {
            if (!player1.ready)
                return;
        }
        start();
    }

    public abstract void playerGoalReached(Player player, GoalReachedMessage goalReachedMessage);

    public boolean canJoin() {
        return (status == 0) && (players.size() < playerCount);
    }

    private synchronized void updateLongWaitTask() {
        if (longWaitTaskScheduled) {
            longWaitTask.cancel();
            longWaitTaskScheduled = false;
        }
        if (players.size() > 1 && players.size() < playerCount) {
            longWaitTask = new TimerTask() {
                @Override
                public void run() {
                    init();
                }
            };
            timer.schedule(longWaitTask, 60000);
            longWaitTaskScheduled = true;
        }
        timer.purge();
    }

    public void join(Player newPlayer) {
        newPlayer.game = this;
        newPlayer.sendTcp(new LobbyFoundMessage());
        for (Player playerInLobby : players) {
            PlayerJoinMessage playerJoinMessage = new PlayerJoinMessage();
            playerJoinMessage.playerModel = playerInLobby.getModel();
            newPlayer.sendTcp(playerJoinMessage);
        }

        players.add(newPlayer);
        if (players.size() == playerCount) {
            init();
        } else {
            PlayerJoinMessage playerJoinMessage = new PlayerJoinMessage();
            playerJoinMessage.playerModel = newPlayer.getModel();
            sendTcpExcept(playerJoinMessage, newPlayer);
        }
        updateLongWaitTask();
    }

    public void exit(Player player) {
        players.remove(player);
        if (players.size() > 0) {
            Collections.sort(players);
            PlayerExitMessage playerExitMessage = new PlayerExitMessage();
            playerExitMessage.nickname = player.playerModel.name;
            sendTcp(playerExitMessage);
        } else {
            end();
        }
        updateLongWaitTask();
    }

    void sendTcp(Object object) {
        for (Player player : players) {
            player.sendTcp(object);
        }
    }

    void sendUdp(Object object) {
        for (Player player : players) {
            player.sendUdp(object);
        }
    }

    private void sendTcpExcept(Object object, Player player) {
        for (Player pl : players) {
            if (pl == player)
                continue;
            pl.sendTcp(object);
        }
    }

    public void sendUdpExcept(Object object, Player player) {
        for (Player pl : players) {
            if (pl == player)
                continue;
            pl.sendUdp(object);
        }
    }

    private void closeConnections() {
        for (Player player : players) {
            player.close();
        }
    }

    public int getId() {
        return id;
    }
}
