package com.lifeapp.shadowracers.server.games;

import com.lifeapp.shadowracers.server.Player;
import com.lifeapp.shadowracers.server.PlayerModel;
import com.lifeapp.shadowracers.server.messages.GameInitMessage;
import com.lifeapp.shadowracers.server.messages.GoalReachedMessage;

import java.util.Collections;
import java.util.TimerTask;

public class GameTotalDrift extends Game {

    private int timeLeft;

    private final int[][] availableSpawns = {
            {1968, 1969, 1967, 1970},
            {1972, 1973, 1971, 1974},
            {1976, 1977, 1975, 1978},
    };

    private int[] spawns;

    public GameTotalDrift(int initialTime) {
        timeLeft = initialTime;
        int index = random.nextInt(availableSpawns.length);
        spawns = availableSpawns[index];
    }

    @Override
    void init() {
        status = 1;
        GameInitMessage gameInitMessage = new GameInitMessage();

        gameInitMessage.spawns = spawns;
        gameInitMessage.mode = 1;
        gameInitMessage.goals = new int[1];
        gameInitMessage.goals[0] = timeLeft;

        gameInitMessage.playerModels = new PlayerModel[players.size()];
        for (int i = 0; i < players.size(); i++) {
            gameInitMessage.playerModels[i] = players.get(i).getModel();
        }
        sendTcp(gameInitMessage);
    }

    @Override
    void gameStarted() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timeLeft--;
                if (timeLeft <= 0) {
                    Collections.sort(players);
                    win(players.get(0));
                    cancel();
                }
            }
        }, 1000, 1000);
    }

    @Override
    public void playerGoalReached(Player player, GoalReachedMessage goalReachedMessage) {
        goalReachedMessage.name = player.playerModel.name;
        player.score += goalReachedMessage.value;
        sendTcp(goalReachedMessage);
    }
}
