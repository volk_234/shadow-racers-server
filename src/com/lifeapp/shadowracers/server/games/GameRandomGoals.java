package com.lifeapp.shadowracers.server.games;

import com.lifeapp.shadowracers.server.Player;
import com.lifeapp.shadowracers.server.PlayerModel;
import com.lifeapp.shadowracers.server.messages.*;

public class GameRandomGoals extends Game {


    private final int goalCount;

    private final int[][] availableSpawns = {
            {1968, 1969, 1967, 1970},
            {1972, 1973, 1971, 1974},
            {1976, 1977, 1975, 1978},
    };

    private int[] spawns;

    public GameRandomGoals(int goalCount) {
        this.goalCount = goalCount;
        int index = random.nextInt(availableSpawns.length);
        spawns = availableSpawns[index];
    }

    @Override
    void init() {
        status = 1;
        GameInitMessage gameInitMessage = new GameInitMessage();

        gameInitMessage.spawns = spawns;
        gameInitMessage.mode = 1;
        gameInitMessage.goals = new int[goalCount];
        for (int i = 0; i < goalCount; i++) {
            gameInitMessage.goals[i] = random.nextInt(maxGoalIndex);
        }

        gameInitMessage.playerModels = new PlayerModel[players.size()];
        for (int i = 0; i < players.size(); i++) {
            gameInitMessage.playerModels[i] = players.get(i).getModel();
        }
        sendTcp(gameInitMessage);
    }

    @Override
    void gameStarted() {

    }

    @Override
    public void playerGoalReached(Player player, GoalReachedMessage goalReachedMessage) {
        goalReachedMessage.name = player.playerModel.name;
        player.score += goalReachedMessage.value;
        if (player.score >= goalCount) {
            win(player);
        } else {
            sendTcp(goalReachedMessage);
        }
    }
}
