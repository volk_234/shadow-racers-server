package com.lifeapp.shadowracers.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.lifeapp.shadowracers.server.games.Game;
import com.lifeapp.shadowracers.server.games.GameLaps;
import com.lifeapp.shadowracers.server.games.GameRandomGoals;
import com.lifeapp.shadowracers.server.games.GameTotalDrift;
import com.lifeapp.shadowracers.server.messages.*;

import java.io.IOException;
import java.util.*;

public class KryServer {
    public static int NET_VERSION = 6;
    private static ArrayList<Game> games = new ArrayList<>();
    private static ArrayList<Player> players = new ArrayList<>();
    private static Random random = new Random();

    private static int games_played = 0;
    private static int players_connected = 0;


    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.bind(8082, 8083);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.start();
        Kryo kryo = server.getKryo();
        kryo.register(ErrorMessage.class);
        kryo.register(RequestLobbyMessage.class);
        kryo.register(GameInitMessage.class);
        kryo.register(GameEndMessage.class);
        kryo.register(GoalReachedMessage.class);
        kryo.register(StateMessage.class);
        kryo.register(PlayerReadyMessage.class);
        kryo.register(GameStartMessage.class);
        kryo.register(ArrayList.class);
        kryo.register(HashMap.class);
        kryo.register(String[].class);
        kryo.register(int[].class);
        kryo.register(float[].class);
        kryo.register(PlayerExitMessage.class);
        kryo.register(PlayerJoinMessage.class);
        kryo.register(LobbyFoundMessage.class);
        kryo.register(PlayerModel.class);
        kryo.register(PlayerModel[].class);

        server.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                Player player = new Player(connection);
                players.add(player);
            }
        });

    }

    public static void joinGame(int id, Player player) {
        players_connected++;

        if (id < 0) {
            Game game = findGame(id);
            if (game != null && game.canJoin()) {
                game.join(player);
                return;
            }
        }

        for (Game game : games) {
            if (game.canJoin()) {
                game.join(player);
                return;
            }
        }

        int mode = random.nextInt(2);
        Game game;
        switch (mode) {
            case 0:
                game = new GameLaps(3);
                break;
            case 1:
                game = new GameTotalDrift(100);
                break;
            default:
                game = new GameLaps(3);
        }
        games.add(game);
        game.join(player);

//        FireBaseUtils.pushNewGameNotification();
    }

    private static void showInfo() {
        System.out.print("\nActive games: " + games.size());
        System.out.print("\nActive players: " + players.size());
        System.out.print("\nTotal games: " + games_played);
        System.out.print("\nTotal players: " + players_connected);
        System.out.print("\n\n");
    }

    public static void removeGame(Game game) {
        games.remove(game);
        games_played++;
        showInfo();
    }

    public static void removePlayer(Player player) {
        players.remove(player);
    }

    private static Game findGame(int id) {
        for (Game game : games) {
            if (game.getId() == id) {
                return game;
            }
        }
        return null;
    }
}
