package com.lifeapp.shadowracers.server.messages;

import com.lifeapp.shadowracers.server.PlayerModel;

/**
 * Created by Александр on 16.03.2018.
 */

public class RequestLobbyMessage {
    public int netVersion;
    public PlayerModel playerModel;
    public int gameId;

    public RequestLobbyMessage() {
    }
}
