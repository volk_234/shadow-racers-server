package com.lifeapp.shadowracers.server.messages;

import com.lifeapp.shadowracers.server.PlayerModel;


public class GameInitMessage {
    public PlayerModel[] playerModels;
    public int mode;
    public int[] spawns;
    public int[] goals;

    public GameInitMessage() {
    }
}
